<?php
require "database.php";

$db = new Database();

$db = new Database();
$db->query('SELECT * FROM agente');
$rows = $db->resultset();

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="bootstrap/assets/ico/favicon.ico">

    <title>Sistema de Gerenciamento de Formulários de Multa</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="bootstrap/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Formulários de Multa</a>
        </div>
        <div class="navbar-collapse collapse">
         
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Buscar...">
          </form>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li><a href="list.php">Formulários</a></li>
			<li class="active"><a href="agentes.php?id=1">Agentes</a></li>
          </ul>
          
        </div>
		<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        
          <h2 class="sub-header">Agentes Registrados</h2>
          <div class="table-responsive">
            <table class="table table-striped">
            
			 <thead>
				<tr>
					<th>Código</th>
					<th>Nome</th>
					<th>Localização Atual</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach($rows as $r) {

				?>
				<tr>
					<td><?php echo $r['codigo']; ?></td>	
					<td><?php echo utf8_encode($r['nome']); ?></td>	
					
					<td>
						<a href="agente.php?id=<?php echo $r['id']; ?>" class="btn btn-info">Visualizar</a>
					</td>
				</tr>

				<?php } ?>
			</tbody>
			<table>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/assets/js/docs.min.js"></script>
  </body>
</html>
