<?php
require "database.php";

$db = new Database();

$db = new Database();
$db->query('SELECT * FROM formulario WHERE id = '.$_GET['id']);
$row = $db->single();

array_walk_recursive($row, 'encode_items');

$json = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='.$row['latitude'].','.$row['longitude'].'&sensor=true');
$json = json_decode($json, true);

function encode_items(&$item, $key)
{
    $item = utf8_encode($item);
}



function base64_to_jpeg($base64_string, $output_file) {
    $ifp = fopen( $output_file, "wb" ); 
    fwrite( $ifp, base64_decode( $base64_string) ); 
    fclose( $ifp ); 
    return( $output_file ); 
}

?>





<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="bootstrap/assets/ico/favicon.ico">

    <title>Sistema de Gerenciamento de Formulários de Multa</title>

    <!-- Bootstrap core CSS -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="dashboard.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]><script src="bootstrap/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Formulários de Multa</a>
        </div>
        <div class="navbar-collapse collapse">
         
          <form class="navbar-form navbar-right">
            <input type="text" class="form-control" placeholder="Buscar...">
          </form>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="active"><a href="list.php">Formulários</a></li>
			<li><a href="agentes.php">Agentes</a></li>
          </ul>
          
        </div>
		<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        
          <h2 class="sub-header">Formulário - N.<?php echo $row['id']?></h2>
          <div class="table-responsive">
		  <table>
		  <tr>
		  <td>
		  <p><strong>Código do Agente:</strong> <?php echo $row['agente']?> </p>
		  <p><strong>Placa:</strong> <?php echo $row['placa']?> </p>
		  <p><strong>UF:</strong> <?php echo $row['uf']?> <strong>Município:</strong> <?php echo $row['municipio']?> </p>
		  <p><strong>Cor:</strong> <?php echo $row['cor']?> <strong>Marca:</strong> <?php echo $row['marca']?> </p>
		  <p><strong>Espécie/Tipo:</strong> <?php echo $row['tipo']?> </p>
		  <p><strong>Categoria do Veículo:</strong> <?php echo $row['categoria']?> </p>
		  <p><strong>Tipificação da infração:</strong> <?php echo $row['infracao']?> </p>
		  <p><strong>Localização:</strong> <?php echo 'Latitude: '.$row['latitude'].' Longitude: '.$row['longitude']?> </p>
		  <p><strong>Endereço: </strong> <?php echo $json['results'][0]['formatted_address']?></p>
		  </td>
		  <td align="center">
		  <p>	  
		  <?php
			$image = base64_to_jpeg( $row['img'], '/var/www/api/tmp'.$row['id'].'.'.$row['format'] );		
			?>
			&nbsp;&nbsp;&nbsp;
			<img src="tmp<?php echo $row['id'].'.'.$row['format'];?>" width="200px" />
		  </p>
		  </td>
		  </tr>
		  </table>
          </div>
        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="bootstrap/assets/js/docs.min.js"></script>
  </body>
</html>
