<?php

saveFormulario($_REQUEST['lat'], $_REQUEST['lon'], $_REQUEST['img'], $_REQUEST['format'], 
$_REQUEST['placa'], $_REQUEST['uf'], $_REQUEST['municipio'], $_REQUEST['cor'], $_REQUEST['marca'], 
$_REQUEST['tipo'], $_REQUEST['categoria'], $_REQUEST['infracao'], $_REQUEST['agente']
);

function saveFormulario($lat, $lon, $img, $format, $placa, $uf, $municipio, $cor, $marca, $tipo, $categoria, $infracao, $agente) {
require "database.php";

$db = new Database();

$db->query('INSERT INTO formulario(latitude, longitude, img, format, placa, uf,municipio,cor, marca, tipo, 
			categoria, infracao, agente) 
			VALUES(:lat, :lon, :img, :format, :placa, :uf, :municipio, :cor, :marca, :tipo, 
				:categoria, :infracao, :agente)');
				
$db->bind(':lat', $lat);
$db->bind(':lon', $lon);
$db->bind(':img', $img);
$db->bind(':format', $format);
$db->bind(':placa', $placa);
$db->bind(':uf', $uf);
$db->bind(':municipio', $municipio);
$db->bind(':cor', $cor);
$db->bind(':marca', $marca);
$db->bind(':tipo', $tipo);
$db->bind(':categoria', $categoria);
$db->bind(':infracao', $infracao);
$db->bind(':agente', $agente);
$db->execute();

header('Content-Type: text/xml');
echo '<?xml version="1.0" encoding="iso-8859-1"?>';
echo '<id>'.$db->lastInsertId().'</id>';

}

/*
function soma($numero1,$numero2) {
  return $numero1+$numero2;
  header('Content-Type: text/xml');
	echo '<?xml version="1.0" encoding="iso-8859-1"?>';
	echo '<resultado>'.soma($_REQUEST['n1'],$_REQUEST['n2']).'</resultado>';
}
*/
?>