package com.vg.transito;
import java.util.List;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.vg.transito.controllers.FormularioController;
import com.vg.transito.controllers.TarefaController;
import com.vg.transito.entities.Formulario;
import com.vg.transito.util.GPSTracker;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class FormView extends Activity {
	public static final String APP_TAG = "com.vg.transito";
	private ListView lvTarefa;
	private Button btNovaTarefa;
	private EditText etNovaTarefa;
	private FormularioController controller;
	
	static final int REQUEST_IMAGE_CAPTURE = 1;
	private static final int CAMERA_REQUEST = 1888; 
    private ImageView imageView;
    
    public double lat;
    public double lon;
    private Button btlocation, btConfirmar;
    public TextView txt;
    private EditText editPlaca, editUf, editMunicipio, editCor, editMarca;
    private Spinner spTipo, spCategoria, spInfracao;
     
    private Formulario formulario;
	
    private GoogleMap googleMap;
    private GPSTracker gps;
    
	@Override 
	public void onCreate(final Bundle bundle) {
		super.onCreate(bundle);
		controller = new FormularioController(this);
		this.setContentView(R.layout.activity_main);
		
		initilizeMap();
		
		this.imageView = (ImageView)this.findViewById(R.id.imageView1);
		Button photoButton = (Button) this.findViewById(R.id.bt);
		photoButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
			        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
			    }
			}
		});
		
		txt = (TextView)this.findViewById(R.id.text1);
		btlocation = (Button) this.findViewById(R.id.btLocation);
		
		gps = new GPSTracker(this);
		
		btlocation.setOnClickListener(new View.OnClickListener() {			
			@Override
			public void onClick(View v) {				
				if(gps.canGetLocation()){ 
					lat = gps.getLatitude();
					lon = gps.getLongitude();
					
					LatLng posicao = new LatLng(lat, lon);
					
					googleMap.moveCamera(CameraUpdateFactory.newLatLng(posicao));
					googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
					googleMap.addMarker(new MarkerOptions().position(posicao));
					
					txt.setText("Latitude: "+lat+" Longitude: "+lon);
				} else {
					txt.setText("Localiza��o indispon�vel");
				}
			}
		});
		
		
		
		btConfirmar = (Button) this.findViewById(R.id.btConfirmar);
		btConfirmar.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				editCor = (EditText) findViewById(R.id.editCor);
				editMarca = (EditText) findViewById(R.id.editMarca);
				editMunicipio = (EditText) findViewById(R.id.editMunicipio);
				editPlaca = (EditText) findViewById(R.id.editPlaca);
				editUf = (EditText) findViewById(R.id.editUf);
				
				spCategoria = (Spinner)findViewById(R.id.spCategoria);
				spInfracao = (Spinner)findViewById(R.id.spInfracao);
				spTipo = (Spinner)findViewById(R.id.spTipo);
				
				
				formulario = new Formulario(
							String.valueOf(lat), String.valueOf(lon), 
							((BitmapDrawable) imageView.getDrawable()).getBitmap(),
							editCor.getText().toString(), editMarca.getText().toString(), editMunicipio.getText().toString(),
							editPlaca.getText().toString(), editUf.getText().toString(), 
							String.valueOf(spCategoria.getSelectedItem()),
							String.valueOf(spInfracao.getSelectedItem()),
							String.valueOf(spTipo.getSelectedItem())	
						);
				
				
				long lastInsertedId = controller.addFormulario(formulario);
				
				Intent intent = new Intent(FormView.this, com.vg.transito.ConfirmView.class);				
				intent.putExtra("lastInsertedId", lastInsertedId);
				FormView.this.startActivity(intent);
			}
		});
	}
	
	private void initilizeMap() {
        if (googleMap == null) {
            googleMap = ((MapFragment) getFragmentManager().findFragmentById(
                    R.id.map)).getMap();
 
            // check if map is created successfully or not
            if (googleMap == null) {
                Toast.makeText(getApplicationContext(),
                        "Erro ao criar mapa", Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }
 
    @Override
    protected void onResume() {
        super.onResume();
        initilizeMap();
    }
	
	
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {  
      
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imageView.setImageBitmap(imageBitmap);
        }
    } 
	/*
	private void populaTarefas() {
		final List tarefas = this.controller.getFormularios();
		Log.d(FormView.APP_TAG, String.format("%d tarefas encontradas ", tarefas.size()));
		this.lvTarefa.setAdapter(new ArrayAdapter (this, android.R.layout.simple_list_item_1, tarefas.toArray(new String[] {})));
		this.lvTarefa.setOnItemClickListener(new OnItemClickListener() {
				@Override 
				public void onItemClick(final AdapterView parent, final View view, final int position, final long id) {
					Log.d(FormView.APP_TAG, String.format("tarefa id: %d e posi��o: %d", id, position));
						
					final TextView v = (TextView) view;
					FormView.this.controller.deleteFormulario(v.getText().toString());
					FormView.this.populaTarefas();
				}
		});
		
		
	}
	
	private final OnClickListener handleNovaTarefaEvent = new OnClickListener() {
		@Override public void onClick(final View view) {
			Log.d(APP_TAG, "bot�o nova tarefa acionado");
			//FormView.this.controller.addTarefa(FormView.this .etNovaTarefa.getText().toString());
			FormView.this.populaTarefas();
		}
	};
	*/
	
	@Override protected void onStart() {
		super.onStart();
	}
	
	@Override protected void onStop() {
		super.onStop();
	}
	
}
