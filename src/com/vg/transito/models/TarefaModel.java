package com.vg.transito.models;

import com.vg.transito.FormView;

import android.content.ContentValues; 
import android.content.Context; 
import android.database.Cursor; 
import android.database.sqlite.SQLiteDatabase; 
import android.database.sqlite.SQLiteOpenHelper; 
import android.util.Log;

public final class TarefaModel {
	private static final String DB_NAME = "tarefas_db"; 
	private static final String TABLE_NAME = "tarefa"; 
	private static final int DB_VERSION = 1; 
	private static final String DB_CREATE_QUERY = "CREATE TABLE " + TarefaModel.TABLE_NAME + " (id integer primary key autoincrement, titulo text not null);"; 
	private final SQLiteDatabase database;
	private final SQLiteOpenHelper helper; 
	
	public TarefaModel(final Context ctx) { 
		this.helper = new SQLiteOpenHelper(ctx, TarefaModel.DB_NAME, null, TarefaModel.DB_VERSION) {
			@Override 
			public void onCreate(final SQLiteDatabase db) { db.execSQL(TarefaModel.DB_CREATE_QUERY); 
		} 
			
		@Override 
		public void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) { 
			db.execSQL("DROP TABLE IF EXISTS " + TarefaModel.TABLE_NAME); this.onCreate(db); 
			} 
		}; 
		
		this.database = this.helper.getWritableDatabase(); 
	} 
	
	public void addTarefa(ContentValues data) { 
		this.database.insert(TarefaModel.TABLE_NAME, null, data); 
	} 
	
	public void deleteTarefa(final String field_params) { 
		this.database.delete(TarefaModel.TABLE_NAME, field_params, null); 
		} 
	
	public Cursor loadAllTarefas() { 
		Log.d(FormView.APP_TAG, "loadAllTarefas()"); 
		final Cursor c = this.database.query(TarefaModel.TABLE_NAME, new String[] { "titulo" }, null, null, null, null, null); 
		return c; 
	}

	
}


