package com.vg.transito.models;

import com.vg.transito.FormView;
import com.vg.transito.entities.Formulario;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class FormularioModel {
	private static final String DB_NAME = "formulario_db"; 
	private static final String TABLE_NAME = "formulario"; 
	private static final int DB_VERSION = 1; 
	private static final String DB_CREATE_QUERY = "CREATE TABLE " + FormularioModel.TABLE_NAME + " (id integer primary key autoincrement, "
																+ "latitude text not null, longitude text not null, img text not null,"
																+ "placa text not null, uf text not null, municipio text not null, "
																+ "cor text not null, marca text not null, tipo text not null,"
																+ "categoria text not null, infracao text not null,"
																+ "agente text not null );"; 
	private final SQLiteDatabase database;
	private final SQLiteOpenHelper helper; 
	
	public FormularioModel(final Context ctx) { 
		this.helper = new SQLiteOpenHelper(ctx, FormularioModel.DB_NAME, null, FormularioModel.DB_VERSION) {
			@Override 
			public void onCreate(final SQLiteDatabase db) { db.execSQL(FormularioModel.DB_CREATE_QUERY); 
		} 
			
		@Override 
		public void onUpgrade(final SQLiteDatabase db, final int oldVersion, final int newVersion) { 
			db.execSQL("DROP TABLE IF EXISTS " + FormularioModel.TABLE_NAME); this.onCreate(db); 
			} 
		}; 
		
		this.database = this.helper.getWritableDatabase(); 
	} 
	
	public long add(ContentValues data) { 
		return this.database.insert(FormularioModel.TABLE_NAME, null, data); 
	} 
	
	public void delete(final String field_params) { 
		this.database.delete(FormularioModel.TABLE_NAME, field_params, null); 
		} 
	
	public Cursor loadAll() { 
		Log.d(FormView.APP_TAG, "loadAll()"); 
		final Cursor c = this.database.query(FormularioModel.TABLE_NAME, new String[] { "titulo" }, null, null, null, null, null); 
		return c; 
	}
	
	public Formulario loadSingle(int id) {
		   
		    Cursor cursor = database.query(FormularioModel.TABLE_NAME, new String[] { "id",
		            "latitude", "longitude", "img", "placa","uf","municipio","cor", "marca", "tipo", 
		            "categoria", "infracao", "agente" }, "id" + "=?",
		            new String[] { String.valueOf(id) }, null, null, null, null);
		    
		    
		    if (cursor != null)
		        cursor.moveToFirst();
		 
		    Formulario formulario = new Formulario();
		    formulario.setId(Integer.parseInt(cursor.getString(0)));
		    formulario.setLatitude(cursor.getString(1));
		    formulario.setLongitude(cursor.getString(2));
		    formulario.setImg(cursor.getString(3));
		    formulario.setPlaca(cursor.getString(4));
		    formulario.setUf(cursor.getString(5));
		    formulario.setMunicipio(cursor.getString(6));
		    formulario.setCor(cursor.getString(7));
		    formulario.setMarca(cursor.getString(8));
		    formulario.setTipo(cursor.getString(9));
		    formulario.setCategoria(cursor.getString(10));
		    formulario.setInfracao(cursor.getString(11));
		    formulario.setAgente(cursor.getString(12));
		    
		    return formulario;
		
	}

}
