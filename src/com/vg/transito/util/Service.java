package com.vg.transito.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.os.AsyncTask;

public class Service extends AsyncTask<String, Void, Boolean> {

	
	@Override
	protected Boolean doInBackground(String... param) {
		// TODO Auto-generated method stub
		
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost("http://200.129.241.92/api/index.php");
		
		List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		pairs.add(new BasicNameValuePair("lat", param[0]));
		pairs.add(new BasicNameValuePair("lon", param[1]));
		pairs.add(new BasicNameValuePair("img", param[2]));
		pairs.add(new BasicNameValuePair("format", param[3]));
		pairs.add(new BasicNameValuePair("placa", param[4]));
		pairs.add(new BasicNameValuePair("uf", param[5]));
		pairs.add(new BasicNameValuePair("municipio", param[6]));
		pairs.add(new BasicNameValuePair("cor", param[7]));
		pairs.add(new BasicNameValuePair("marca", param[8]));
		pairs.add(new BasicNameValuePair("tipo", param[9]));
		pairs.add(new BasicNameValuePair("categoria", param[10]));		
		pairs.add(new BasicNameValuePair("infracao", param[11]));
		pairs.add(new BasicNameValuePair("agente", param[12]));	
		try {
			post.setEntity(new UrlEncodedFormEntity(pairs));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			HttpResponse response = client.execute(post);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
