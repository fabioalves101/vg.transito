package com.vg.transito;

import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;

public class mvcSplash extends Activity {

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ui_splash);
		Thread timer = new Thread() {
			public void run() {
				try {
					sleep(1000);
				}
				catch (InterruptedException e) {
					e.printStackTrace();
				}
				finally {
					Intent intent = new Intent(mvcSplash.this, com.vg.transito.FormView.class);				
					mvcSplash.this.startActivity(intent);
					
				}
			}
		};
		timer.start();
		}
	}
