package com.vg.transito.entities;

import com.vg.transito.util.ImageBase64;

import android.graphics.Bitmap;
import android.text.Editable;

public class Formulario {
	public Formulario() {}
	
	public Formulario(int id, String lat, String lon, String img) {
		this.id = id;
		this.latitude = lat;
		this.longitude = lon;		
		this.img = img;
	}
	
	public Formulario(String lat, String lon, String img) {
		this.latitude = lat;
		this.longitude = lon;		
		this.img = img;
	}

	public Formulario(String lat, String lon, Bitmap imgBmp,
			String editCor, String editMarca, String editMunicipio, 
			String editPlaca, String editUf, String spCategoria, 
			String spInfracao, String spTipo) {
	
		this.latitude = lat;
		this.longitude = lon;	
		this.img = ImageBase64.encodeTobase64(imgBmp);
		this.cor = editCor; 
		this.marca = editMarca;
		this.municipio = editMunicipio;
		this.placa = editPlaca;
		this.uf = editUf;
		this.categoria = spCategoria;
		this.infracao = spInfracao;
		this.tipo = spTipo;
	}
	
	public Formulario(String lat, String lon, String img,
			String editCor, String editMarca, String editMunicipio, 
			String editPlaca, String editUf, String spCategoria, 
			String spInfracao, String spTipo) {
	
		this.latitude = lat;
		this.longitude = lon;	
		this.img = img;
		this.cor = editCor; 
		this.marca = editMarca;
		this.municipio = editMunicipio;
		this.placa = editPlaca;
		this.uf = editUf;
		this.categoria = spCategoria;
		this.infracao = spInfracao;
		this.tipo = spTipo;
	}
	
	public Formulario(int id, String lat, String lon, String img,
			String editCor, String editMarca, String editMunicipio, 
			String editPlaca, String editUf, String spCategoria, 
			String spInfracao, String spTipo) {
		
		this.latitude = lat;
		this.longitude = lon;
		this.id = id;
		this.img = img;
		this.cor = editCor; 
		this.marca = editMarca;
		this.municipio = editMunicipio;
		this.placa = editPlaca;
		this.uf = editUf;
		this.categoria = spCategoria;
		this.infracao = spInfracao;
		this.tipo = spTipo;
	}


	private int id;
	private String latitude;
	private String longitude;	
	private String img;
	
	private String placa, uf, municipio, cor, marca, tipo, categoria, infracao, agente;
	
	public String getAgente() {
		return agente;
	}

	public void setAgente(String agente) {
		this.agente = agente;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getInfracao() {
		return infracao;
	}

	public void setInfracao(String infracao) {
		this.infracao = infracao;
	}

	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

}
