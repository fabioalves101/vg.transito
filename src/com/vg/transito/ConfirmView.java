package com.vg.transito;

import java.util.List;

import com.vg.transito.controllers.FormularioController;
import com.vg.transito.entities.Formulario;
import com.vg.transito.util.ImageBase64;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

public class ConfirmView extends Activity {
	private TextView txtLocalizacao;
	private ImageView imageView;
	private TextView txtPlaca, txtUf, txtMunicipio, txtCor, txtMarca, txtTipo, txtCategoria, txtInfracao;
	private Button btVoltar;

	private FormularioController controller;

	@Override 
	public void onCreate(final Bundle bundle) {
		super.onCreate(bundle);
		controller = new FormularioController(this);
		this.setContentView(R.layout.confirm_form);		
		
		long lastInsertedId = getIntent().getLongExtra("lastInsertedId", 0);	
		
		final Formulario formulario = this.controller.getFormulario(lastInsertedId);
		
		txtLocalizacao = (TextView)this.findViewById(R.id.txtLocalizacao);
		txtLocalizacao.setText("Latitude: "+formulario.getLatitude()+" Longitude: "+formulario.getLongitude());
		
		txtPlaca = (TextView)this.findViewById(R.id.txtPlaca);
		txtPlaca.setText("Placa: "+formulario.getPlaca());
		
		txtUf = (TextView)this.findViewById(R.id.txtUf);
		txtUf.setText("UF: "+formulario.getUf());
		
		txtMunicipio = (TextView)this.findViewById(R.id.txtMunicipio);
		txtMunicipio.setText("Munic�pio: "+formulario.getMunicipio());
		
		txtCor = (TextView)this.findViewById(R.id.txtCor);
		txtCor.setText("Cor: "+formulario.getCor());
				
		txtMarca = (TextView)findViewById(R.id.txtMarca);
		txtMarca.setText("Marca: "+formulario.getMarca());
		
		txtTipo = (TextView)findViewById(R.id.txtTipo);
		txtTipo.setText("Tipo: "+formulario.getTipo());
		
		txtCategoria = (TextView)findViewById(R.id.txtCategoria);
		txtCategoria.setText("Categoria: "+formulario.getCategoria());
		
		txtInfracao = (TextView)findViewById(R.id.txtInfracao);
		txtInfracao.setText("Tipifica��o da Infra��o: "+formulario.getInfracao());
				
		imageView = (ImageView) this.findViewById(R.id.imageView);
		imageView.setImageBitmap(ImageBase64.decodeBase64(formulario.getImg()));
		
		btVoltar = (Button)this.findViewById(R.id.btVoltar);
		btVoltar.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {

				Intent intent = new Intent(ConfirmView.this, com.vg.transito.FormView.class);				
				ConfirmView.this.startActivity(intent);
			}
		});
	}
	
	
	
	@Override protected void onStart() {
		super.onStart();
	}
	
	@Override protected void onStop() {
		super.onStop();
	}
}
