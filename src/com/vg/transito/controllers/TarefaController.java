package com.vg.transito.controllers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;
import com.vg.transito.models.*;

public class TarefaController {
	private TarefaModel model;
	private List tarefas;
	
	public TarefaController(Context app_context) {
		tarefas = new ArrayList();
		model = new TarefaModel(app_context);
	}
	
	public void addTarefa(final String titulo) {
		final ContentValues data = new ContentValues();
		data.put("titulo", titulo);
		model.addTarefa(data);
	}
	
	public void deleteTarefa(final String titulo) {
		model.deleteTarefa("titulo='" + titulo + "'");
	}
	public void deleteTarefa(final long id) {
		model.deleteTarefa("id='" + id + "'");
	}
	public void deleteAllTarefa() {
		model.deleteTarefa(null);
	}
	public List getTarefas() {
		Cursor c = model.loadAllTarefas();
		tarefas.clear();
		if (c != null) {
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			tarefas.add(c.getString(0));
			c.moveToNext();
		}
		c.close();
	}
		return tarefas;
	}
}