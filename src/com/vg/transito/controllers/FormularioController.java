package com.vg.transito.controllers;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.R.integer;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;

import com.vg.transito.entities.Formulario;
import com.vg.transito.models.FormularioModel;
import com.vg.transito.util.ImageBase64;
import com.vg.transito.util.Service;

public class FormularioController {
	private FormularioModel model;
	private List tarefas;
	
	public FormularioController(Context app_context) {
		tarefas = new ArrayList();
		model = new FormularioModel(app_context);
	}
	
	public long addFormulario(String latitude, String longitude, Bitmap image) {
		
		String imageData = ImageBase64.encodeTobase64(image);
		
		ContentValues data = new ContentValues();
		data.put("latitude", latitude);
		data.put("longitude", longitude);
		data.put("img", imageData);
		long id = model.add(data);
		
		if(id != 0) {
			try {
				this.sincronizar(new Formulario(latitude, longitude, imageData));

				return id;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();				

				return id;
			}
		} else {
			return 0;
		}
	}
	
	public long addFormulario(Formulario formulario) {
		
		ContentValues data = new ContentValues();
		data.put("latitude", formulario.getLatitude());
		data.put("longitude", formulario.getLongitude());
		data.put("img", formulario.getImg());
		data.put("placa", formulario.getPlaca());
		data.put("uf", formulario.getUf());
		data.put("municipio", formulario.getMunicipio());
		data.put("cor", formulario.getCor());
		data.put("marca", formulario.getMarca());
		data.put("tipo", formulario.getTipo());
		data.put("categoria", formulario.getCategoria());		
		data.put("infracao", formulario.getInfracao());
		data.put("agente", "123");		
		long id = model.add(data);
				
		
		if(id != -1) {
			try {
				this.sincronizar(new Formulario(formulario.getLatitude(), formulario.getLongitude(), formulario.getImg(),
						formulario.getCor(), formulario.getMarca(), formulario.getMunicipio(),
						formulario.getPlaca(), formulario.getUf(), formulario.getCategoria(),
						formulario.getInfracao(), formulario.getTipo()
						));

				return id;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();				

				return id;
			}
		} else {
			return 0;
		}
	}
	
	public void deleteFormulario(final String agente) {
		model.delete("agente='" + agente + "'");
	}
	public void deleteFormulario(final long id) {
		model.delete("id='" + id + "'");
	}
	public void deleteAll() {
		model.delete(null);
	}
	public List getFormularios() {
		Cursor c = model.loadAll();
		tarefas.clear();
		if (c != null) {
		c.moveToFirst();
		while (c.isAfterLast() == false) {
			tarefas.add(c.getString(0));
			c.moveToNext();
		}
		c.close();
	}
		return tarefas;
	}
	
	public Formulario getFormulario(long id) {
		return model.loadSingle(Integer.parseInt(String.valueOf(id)));
	}

	public void sincronizar(Formulario formulario) throws ClientProtocolException, IOException {
		new Service().execute(formulario.getLatitude(), formulario.getLongitude(), formulario.getImg(), "bmp",
				formulario.getPlaca(),
				formulario.getUf(),
				formulario.getMunicipio(),
				formulario.getCor(),
				formulario.getMarca(),
				formulario.getTipo(),
				formulario.getCategoria(),		
				formulario.getInfracao(),
				"123");
	}
}
